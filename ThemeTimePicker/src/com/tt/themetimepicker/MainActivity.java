package com.tt.themetimepicker;

import java.util.Calendar;

import com.tt.themetimepicker.R;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

/**
 * 测试页面 <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author Kyson www.hizhaohui.cn
 */
public class MainActivity extends Activity {
    private ThemeTimePicker mTimePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTimePicker = (ThemeTimePicker) this.findViewById(R.id.timepicker);
        mTimePicker.setIs24HourView(true);
        //设置一个默认显示的时间(现在)
        Calendar calendar = Calendar.getInstance();
        mTimePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        mTimePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));

        changeColor(
                getResources().getColor(R.color.material_green),
                getResources().getDrawable(
                        R.drawable.tl_picker_divider_green_shape));

    }

    private void changeColor(int color, Drawable divider) {
        mTimePicker.setupTheme(color, divider);
    }

    public void blue(View view) {
        changeColor(
                getResources().getColor(R.color.material_blue),
                getResources().getDrawable(
                        R.drawable.tl_picker_divider_blue_shape));
    }

    public void green(View view) {
        changeColor(
                getResources().getColor(R.color.material_green),
                getResources().getDrawable(
                        R.drawable.tl_picker_divider_green_shape));
    }

    public void red(View view) {
        changeColor(
                getResources().getColor(R.color.material_red),
                getResources().getDrawable(
                        R.drawable.tl_picker_divider_red_shape));
    }

}
